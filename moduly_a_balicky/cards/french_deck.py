import collections
from invoke import task
from flask import Flask
import pkg_resources
import logging
import yaml
import logging.config

logger = logging.getLogger(__name__)


Card = collections.namedtuple("Card", ["rank", "suit"])


class CardError(Exception):
    pass


class FrenchDeck:
    ranks = [str(n) for n in range(2, 11)] + list("JQKA")
    suits = "spades diamonds clubs hearts".split()

    def __init__(self):
        self._cards = [Card(rank, suit) for suit in self.suits for rank in self.ranks]

    def __len__(self):
        return len(self._cards)

    def __getitem__(self, position):
        return self._cards[position]

    def __setitem__(self, index, other):
        if not isinstance(other, Card):
            raise CardError(f"You can only set a Card object, not {type(other)}")
        self._cards[index] = other


@task
def get_card(c, s, num=1, log_config=None):

    if log_config is None:
        log_config = pkg_resources.resource_filename("cards", "logging.yaml")

    with open(log_config, "rt") as f:
        config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)

    logger.debug("log_config: %s", log_config)

    deck = FrenchDeck()
    logger.info("French deck created")
    print(s)
    for i in range(num):
        print(deck[i])


@task
def http(c):

    app = Flask(__name__)

    @app.route("/")
    def hello_world():
        deck = FrenchDeck()
        cards = []
        for i in range(5):
            cards.append(str(deck[i]))
        return "\n".join(cards)

    app.run()


@task
def logging_tree(c):
    import logging_tree

    logging_tree.printout()
