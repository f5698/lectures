from setuptools import setup

setup(
    name='cards',
    version='0.0.1',
    packages=['cards'],
    install_requires=[
        'invoke',
        'flask',
        'pyaml'
    ],
    entry_points={
        'console_scripts': ['cards = cards.main:program.run']
    },
    include_package_data=True,
    package_data={
        "cards": ["*.yaml"]
    }
)
