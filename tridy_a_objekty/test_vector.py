from vector import Vector


def test_sum():
    v1 = Vector(2, 4)
    v2 = Vector(2, 1)

    assert v1 + v2 == Vector(4, 5)


def test_sum_fail():
    v1 = Vector(1, 1)
    v2 = Vector(1, 1)

    assert v1 + v2 != Vector(3, 3)


def test_abs():
    v = Vector(3, 4)
    assert abs(v) == 5


def test_scalar_mul():
    v = Vector(3, 4)
    assert v * 2.0 == Vector(6.0, 8.0)
