from math import hypot


class Vector:
    def __init__(self, x=0, y=0):
        self.x = float(x)
        self.y = float(y)

    def f(self):
        return 15

    def __repr__(self):
        return "Vector(%r, %r)" % (self.x, self.y)

    def __str__(self) -> str:
        return "(%r, %r)" % (self.x, self.y)

    def __abs__(self):
        return hypot(self.x, self.y)

    def __bool__(self):
        return bool(abs(self))

    def __eq__(self, o: object) -> bool:
        return self.x == o.x and self.y == o.y

    def __add__(self, other):
        x = self.x + other.x
        y = self.y + other.y
        return Vector(x, y)

    def __mul__(self, scalar):
        return Vector(self.x * scalar, self.y * scalar)
    
    def has_same_direction(self, o):
        
