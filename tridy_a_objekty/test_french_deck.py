from french_deck import Card, FrenchDeck


def test_len():
    deck = FrenchDeck()
    assert len(deck) == 52


def test_first_card():
    deck = FrenchDeck()
    assert deck[0] == Card("2", "spades")


def test_last_card():
    deck = FrenchDeck()
    assert deck[-1] == Card("A", "hearts")
